# Implementation of LFSR algorithm
> Project implements a random number generation LFSR

## Table of Contents
* [General Info](#general-information)
* [Technologies Used](#technologies-used)
* [Features](#features)
* [Project Status](#project-status)
* [Contact](#contact)


## General Information
- This project was realized on IV semester of studies.
- Program generate random numbers choose by user
- Program gives information about accuracy of randomness


## Technologies Used
- Python - version 3.11
- PyCharm Community Edition


## Features
Provided features:
- Generate numbers from seed (hex, dec, oct)
- Need seed and start vector 
- Description show generate numbers and accuracy of randomness



## Project Status
Project is: _no longer being worked on_. 


## Contact
Created by Piotr Hajduk

