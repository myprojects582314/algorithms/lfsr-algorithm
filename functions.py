# Piotr Hajduk s189459
import numpy as np


def read_file():
    file_name = input("Podaj nazwe pliku zawierajacego seed oraz wektor p\n")
    try:
        file = open(file_name, 'r')
        seed = file.readline()[:-1]
        p = file.readline()
        not_bin(seed,p)
        if len(seed) != len(p):
            print("Dlugosci seed oraz p nie sa takie same, zredukowano do mnijeszej dlugosci\n")
            seed = lens_seed_p(seed, p)[0]
            p = lens_seed_p(seed, p)[1]
        return seed, p
    except:
        print(f"Blad w otwarciu pliku {file_name} sproboj ponownie:\n")
        return read_file()


def lens_seed_p(seed,p):
    if len(seed) != len(p):
        if len(seed) < len(p):
            p = p[0:len(seed)]
        else:
            seed = seed[0:len(p)]
    return seed, p


def if_bin_text(text):
    k = 0
    for i in text:
        if i != '0' and i != '1':
            k = 1
    if k == 1:
        return False
    elif k == 0:
        return True


def not_bin(seed,p):
    if if_bin_text(seed)==False or if_bin_text(p)==False:
        print("Nieprawidlowy seed lub p wpisz dobry plik txt lub popraw aby dane były zapisem binarnym\n")
        read_file()


def fill_text(text,n):
    while len(text) % n != 0:
        text += '0'
    return text


def create_boxes(text,n):
    text = fill_text(text, n)
    box = []
    for i in range(0, len(text), n):
        box_word = text[i:i + n]
        box.append(box_word)
    return box


def bin_to_hex(text):
    boxes = create_boxes(text, 4)
    numbers = []
    result = ''
    for box in boxes:
        box = hex(int(box, 2))[2:]
        numbers.append(box)
        result += box + ' '
    return result, numbers


def bin_to_oct(text):
    boxes = create_boxes(text, 3)
    numbers = []
    result = ''
    for box in boxes:
        box = oct(int(box,2))[2:]
        numbers.append(box)
        result += box+' '
    return result, numbers


def bin_to_dec(text):
    boxes = create_boxes(text, 5)
    numbers = []
    result = ''
    for box in boxes:
        box = int(int(box,2))
        numbers.append(box)
        result += str(box)+' '
    return result, numbers


def generate_next_seed(seed,p,n):
    result_temp = seed
    result = seed
    for i in range(n):
        before_mod = 0
        for j in range(len(p)):
            before_mod += int(p[j])*int(result_temp[j])
        result = str(before_mod%2) + result
        result_temp = result[:len(p)]
    return result


def save_to_file(seed, p, text, accuracy, change_text, system):
    file_name = input("Podaj nazwe pliku do ktorego chcesz zapisac wygenerowany ciag liczb\n")
    if file_name[len(file_name)-4:] != '.txt':
        print("Podany plik nie jets plikiem txt sproboj ponownie")
        save_to_file(seed, p, text, accuracy, change_text, system)
    else:
        file = open(file_name, 'w')
        file.write(f'Seed: {seed}\nWektor p: {p}\nWygenerowany ciag: {text}\n'
                   f'Dokladnosc: {accuracy}\nWygenerowane liczby: {change_text}\n'
                   f'Wybrany system liczbowy: {system}')
        file.close()


def random_check_hex(numbers):
    n = ['0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f']
    n_vals = np.zeros(16)
    for number in numbers:
        for val in range(len(n)):
            if n[val] == number:
                n_vals[val] += 1
    n_vals /= len(numbers)
    m = max(n_vals)
    n = min(n_vals)

    return round(m-n,5)


def random_check_oct(numbers):
    n = ['0','1','2','3','4','5','6','7']
    n_vals = np.zeros(8)
    for number in numbers:
        for val in range(len(n)):
            if n[val] == str(number):
                n_vals[val] += 1
    n_vals /= len(numbers)
    m = max(n_vals)
    n = min(n_vals)

    return round(m-n,5)


def random_check_dec(numbers):
    n = np.arange(0,32,1)
    n_vals = np.zeros(32)
    for number in numbers:
        for val in range(len(n)):
            if n[val] == number:
                n_vals[val] += 1
    n_vals /= len(numbers)
    m = max(n_vals)
    n = min(n_vals)

    return round(m-n,5)


def random_check_bin(numbers):
    n = ['0','1']
    n_vals = np.zeros(2)
    for number in numbers:
        for val in range(len(n)):
            if n[val] == number:
                n_vals[val] += 1
    n_vals /= len(numbers)
    m = max(n_vals)
    n = min(n_vals)

    return round(m-n,5)


def bin_seperate_space(text):
    result = ''
    for i in text:
        result +=i+' '
    return result

