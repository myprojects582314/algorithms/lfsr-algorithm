# Piotr Hajduk s189459
import functions as f

try:
    print("Witam w programie do generowania liczb pseudolosowych LFSR\n"
          "Podany plik wejsciowy powinien zawierac dwie liniki tej samej dlugosci:\n"
          "\t1. ziarno seed (w zapisie binarnym)\n"
          "\t2. wektor p definiujacy generowanie (w zapisie binarnym)\n")
    datas = f.read_file()
    seed = datas[0]
    p = datas[1]
    if len(seed) < 25:
        print(f"Seed: {seed}\nWektor p: {p}")
    else:
        print("Wybrane ziarno oraz wektor do 25 znakow:")
        print(f"Seed: {seed[:25]}\nWektor p: {p[:25]}")
    print("Podaj liczbe ile liczb chcesz wygenerowac:")
    n = int(input())
    while n < 0:
        print(f"Podana liczba {n} nie jest liczba naturalna, podaj liczbe ponownie:")
        n = int(input())
    gen_result = f.generate_next_seed(seed, p, n)
    if len(gen_result) < 50:
        print(f'Wygenerowany ciag liczb: {gen_result}')
    else:
        print(f"Fragment wygenerowanych liczb do 50 znakow: {gen_result[:50]}")
    print("W jakim systemie liczbowym chcesz zapisac wynik wynik:\n"
          "\tWybierz 0 dla systemu binarnego\n"
          "\tWybierz 1 dla systemu osemkowego\n"
          "\tWybierz 2 dla systemu dziesietnego\n"
          "\tWybierz 3 dla systemu szesnastkowego\n"
          "Wraz z wyborem podana bedzie miara losowosci (nie dla binarnego)\n"
          "im liczba zbliza sie do 0 liczby sa lepiej wylosowane (zakres 0-1)\n")
    choices = ['0', '1', '2', '3', '4']
    choice = input("Wpisz cyfre zgodnie z podanym menu:     ")
    while choice not in choices:
        choice = input("Niepoprawny wybor podaj poprawna liczbe:    ")
    if choice == '0':
        system = "binarny"
        numbers = f.create_boxes(gen_result, 1)
        result = f.bin_seperate_space(gen_result)
        accuracy = f.random_check_bin(numbers)
        print(f'Dokladnosc generowania: {accuracy}')
        print(f'Wygenerowane liczby w systemie binranym zakres (0-1): {result}')
        f.save_to_file(seed, p, gen_result, accuracy, result, system)
    elif choice == '1':
        system = "osemkowy"
        data = f.bin_to_oct(gen_result)
        numbers = data[1]
        result = data[0]
        accuracy = f.random_check_oct(numbers)
        print(f'Dokladnosc generowania: {accuracy}')
        print(f'Wygenerowane liczby w systemie osemkowym zakres (0-7): {result}')
        f.save_to_file(seed, p, gen_result, accuracy, result, system)
    elif choice == '2':
        system = "dziesietny"
        data = f.bin_to_dec(gen_result)
        numbers = data[1]
        result = data[0]
        accuracy = f.random_check_dec(numbers)
        print(f'Dokladnosc generowania: {accuracy}')
        print(f'Wygenerowane liczby w systemie dziesietnym (zakres 0-31): {result}')
        f.save_to_file(seed, p, gen_result, accuracy, result, system)
    elif choice == '3':
        system = "szesnastkowy"
        data = f.bin_to_hex(gen_result)
        numbers = data[1]
        result = data[0]
        accuracy = f.random_check_hex(numbers)
        print(f'Dokladnosc generowania: {accuracy}')
        print(f'Wygenerowane liczby w systemie szesnastkowym (zakres 0-f): {result}')
        f.save_to_file(seed, p, gen_result, accuracy, result, system)
except:
    print(f"Podany znak nie jest akceptowalna liczba: brak mozliwosci generowania\n")
